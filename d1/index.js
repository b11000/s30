// ODM
// Object Document Mapper is a tool that translates objects in code to document for use in document-based databases such as "MONGODB ODM"
// 
// Moongoose
// An ODM library that manages data relationships, validates schemas,
// and simplifies MongoDB document manipulation via the use of models.
// 
// Models
// A programming interface for querying or manipulating

const express = require("express");
// Mongoose is a package that allows creation of schemas to model our data structures and to have an access to a number of methods for manipulating our database.
const mongoose = require("mongoose");
const app = express();
const port = 3001;
// MongoDB Connection
// Connecting to MongoDB Atlas

mongoose.connect("mongodb+srv://admin:admin@cluster0.gguoi.mongodb.net/batch144-to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

// Set notifications for connection success or failure
let db = mongoose.connection;
// if error occured, output in the console
db.on("Error", console.error.bind(console, "connection error"))
// if the connection is successful, output in the console
db.once("open",()=>console.log("We're connected to the cloud database."))

app.use(express.json())
app.use(express.urlencoded({extended:true}))

// Mongoose Schemas

// Schemas determine the structure of the documents to be written in the database
// it acts as a blueprints to our data
// We will use Schema() constructor of the Mongoose module to craete a new Schema object
const taskSchema = new mongoose.Schema({//this is the model in MVC
	name: String,
	status: {
		type: String,
		// Default values are the predefines values for a field if we don't put any value
		default: "pending"
	}
})
const Task = mongoose.model("Task", taskSchema)
//Models are what allows us to gain access to methods that will perform CRUD functions.
//Models must be in singular form and capitalized following the MVC approach for naming conventions
//firstparameter of the mongoose model method indicates the collection in where to store the data

// Routes
// Create a new task
/* Business Logic
1. Add a functionality to check if there are duplicate tasks //Check if it has duplicate task
	-if the task already exists, return there is a duplicate
	-if the task doesn't exists, we can add it in the database
2. The task data will be coming from the request's body//task will origin is from client specifically in the body
3. Create new Task object with properties that we need
4. Then save the data
*/
app.post("/tasks",(req, res)=>{
	// Check if there are duplicates tasks
	// findOne() is a mongoose method that acts similar to "find"
	// it returns the first document that matches the research criteria
	Task.findOne( {name: req.body.name},(error, result) => {
		// If a document was found and the document's name matches the information sent via client/postman
		if(result !== null && result.name == req.body.name){
			// return a message to the client/postman
			return res.send("Duplicate task found")
		}else{
			// if no document was found
			// Create a new task and save it to the database
			let newTask = new Task({
				name: req.body.name
			})

	newTask.save((saveErr, savedTask) => {
	// if there are errors in saving
				if(saveErr){
				// Will print any errors found in the console
				// saveErr is an error object that will contain details about the error
				// Errors normally come as an object data type
				return console.error(saveErr)
				}else{
				// no error found while creating the document
				// Return a status code of 201 for successful creation
				return res.status(201).send("New Task Created")
				}
			})
		}
	} )//error function will handle error handling
})

/*Retrieving all data
Business Logic 
1. Retrieve all the documents using the find()
2. If an error is encountered, print the error
3. If no erros are found, send a success status back to the client and return an array of documents

*/

app.get("/tasks", (req,res)=>{
	//an empty "{}" means it returns all the documents and stores them in the "result" parameter of the call back function
	Task.find({},(err,result)=>{
		if(err){
			return console.log(err)
		}else{
			return res.status(200).json({
				data: result
			})
		}
	})
})

// Activity:
// 1. Create a User schema.
// 2. Create a User model.
// 3. Create a POST route that will access the "/signup" route that will create a user.
// 4. Process a POST request at the "/signup" route using postman to register a user.
// 5. Create a GET route that will return all users.
// 6. Process a GET request at the "/users" route using postman.
// 7. Create a git repository named S30.
// 8. Initialize a local git repository, add the remote link and push to git with the commit message of s30 Activity.
// 9. Add the link in Boodle named Express js Data Persistence via Mongoose ODM.

// Register a user(Business Logic)
// 1. Find if there are duplicate user
// 		-If user already exists, we return an error
// 		-If user doesn't exist, we add it in the database
// 			2.If the username and password are both not blank
//				-if blank, send response "BOTH username and password must be provided"
//				- both condition has been met, create a new object.
//				- Save the new object
//					-if error, return an error message
//					-else, response a status 201 and "New User Registered"
const userSchema = new mongoose.Schema({
	username: String,
	password: String
})
const User = mongoose.model("User", userSchema)

app.post("/signup",(req, res)=>{
	User.findOne( {username: req.body.username}, (error, result)=> {
		if(result !== null && result.username == req.body.username){
			return res.send("Duplicate user found")
		}else if(req.body.username == null || req.body.password == null) {
        return res.send("Please provide BOTH username and password");
      	}else{
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			})
		newUser.save((signupErr, signupUser)=>{
			if(signupErr){
				return console.error(signupErr)
			}else{
				return res.status(201).send("New User Created Congratulations!")
				}
			})
		}
	})
})

app.get("/users", (req,res)=>{
	User.find({},(err,result)=>{
		if(err){
			return console.log(err)
		}else{
			return res.status(200).json({
				data: result
			})
		}
	})
} )

app.listen(port, () => console.log(`Server is running at port ${port}.`))
